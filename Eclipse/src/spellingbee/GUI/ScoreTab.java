package spellingbee.GUI;

import javafx.geometry.Insets;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;
/**
 * @author Zacharie Makeen
 */
public class ScoreTab extends Tab {

	private Client client;
	String[] brackets;
	private Text rank5;
	private Text rank4;
	private Text rank3;
	private Text rank2;
	private Text rank1;
	private Text score;
	/**
	 * This constructor builds the gui for the score tab.
	 * 
	 * @param client the client object representing the Client class.
	 */
	public ScoreTab(Client client) {

		super("Score");
		this.client = client;
		
		brackets = client.sendAndWaitMessage("getBrackets").split(", ");

		this.rank5 = new Text("Queen Bee");
		this.rank4 = new Text("Genius");
		this.rank3 = new Text("Amazing");
		this.rank2 = new Text("Good");
		this.rank1 = new Text("Getting Started");
		Text scoreLabel = new Text("Current Score");

		this.rank5.setFill(Color.GREY);
		this.rank4.setFill(Color.GREY);
		this.rank3.setFill(Color.GREY);
		this.rank2.setFill(Color.GREY);
		this.rank1.setFill(Color.GREY);
		scoreLabel.setFill(Color.BLACK);

		Text milestone5 = new Text(brackets[4]);
		Text milestone4 = new Text(brackets[3]);
		Text milestone3 = new Text(brackets[2]);
		Text milestone2 = new Text(brackets[1]);
		Text milestone1 = new Text(brackets[0]);
		this.score = new Text("0");

		this.score.setFill(Color.RED);

		GridPane grid = new GridPane();

		grid.add(rank5, 0, 0, 1, 1);
		grid.add(rank4, 0, 1, 1, 1);
		grid.add(rank3, 0, 2, 1, 1);
		grid.add(rank2, 0, 3, 1, 1);
		grid.add(rank1, 0, 4, 1, 1);
		grid.add(scoreLabel, 0, 5, 1, 1);
		grid.add(milestone5, 1, 0, 1, 1);
		grid.add(milestone4, 1, 1, 1, 1);
		grid.add(milestone3, 1, 2, 1, 1);
		grid.add(milestone2, 1, 3, 1, 1);
		grid.add(milestone1, 1, 4, 1, 1);
		grid.add(score, 1, 5, 1, 1);
		grid.setHgap(10);
		grid.setVgap(10);
        grid.setPadding(new Insets(10,10,10,10));
        grid.setMinSize(400, 400);
        grid.setMaxSize(700, 700);

		this.setContent(grid);
	}
	/**
	 * This method updates the value of the score as well as the colours
	 * of the ranks whenevr the user guesses a word.
	 */
	public void refresh() {

		String currentScore = client.sendAndWaitMessage("getScore");
		this.score.setText(currentScore);
		if(Integer.parseInt(currentScore) >= Integer.parseInt(this.brackets[0])) {

			this.rank1.setFill(Color.BLACK);
		} else if(Integer.parseInt(currentScore) >= Integer.parseInt(this.brackets[1])) {
			
			this.rank2.setFill(Color.BLACK);
		} else if(Integer.parseInt(currentScore) >= Integer.parseInt(this.brackets[2])) {
			
			this.rank3.setFill(Color.BLACK);
		} else if(Integer.parseInt(currentScore) >= Integer.parseInt(this.brackets[3])) {
			
			this.rank4.setFill(Color.BLACK);
		} else if(Integer.parseInt(currentScore) == Integer.parseInt(this.brackets[4])) {
			
			this.rank5.setFill(Color.BLACK);
		}
	}
}