package spellingbee.GUI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import spellingbee.network.Client;
import spellingbee.server.SpellingBeeGame;

/**
 * @author Phan Hieu Nghia
 */
public class GameEngine implements EventHandler<ActionEvent> {
	
	//private fields
	private TextField words;
	private String letters;
	private String action;
	private Client client;
	private Label msg;
	private Label points;
	
	//Constructors for each buttons
	
	/**
	 * This constructor text input the private fields and return the
	 * corresponding letter buttons that was pushed 
	 * @param TextField words, String letters, String action
	 * @return words, letters and actions
	 */ 
	public GameEngine(TextField words, String letters, String action) {
		this.words = words;
		this.letters = letters;
		this.action = action;
	}
	
	/**
	 * This constructor text input the private fields and return the
	 * actions that will be set on each utility buttons
	 * @param TextField words, String action
	 * @return words, action
	 */
	public GameEngine(TextField words, String action) {
		this.action = action;
		this.words = words;
	}
	
	/**
	 * This constructor text input the private fields and return the
	 * the messages and the points for each word entered
	 * @param Label msg, Label points, TextField words, Client client, String action
	 * @return msg, points, words, client, action
	 */
	public GameEngine(Label msg, Label points, TextField words, Client client, String action) {
		this.msg = msg;
		this.points = points;
		this.words = words;
		this.client = client;
		this.action = action;
	}
	
	public GameEngine(Label points, String action) {
		
		this.points = points;
		this.action = action;
	}
	
	@Override
	//ActionEvent method to handle the event listeners for each buttons
	public void handle(ActionEvent arg) {
		StringBuilder sb;
		
		if (action.equals("setLetters")) {
			
			words.setText(words.getText() + letters);
		} else if (action == "Clear") {
			
			words.setText("");
		} else if (action.equals("Delete")) {
			
			letters = words.getText();
			sb = new StringBuilder(letters);
			if (sb.toString().length() > 0) {
				sb.deleteCharAt(letters.length() - 1);
				words.setText(sb.toString());
			}
		} else if (action.equals("Submit")) {
			
			String responce = client.sendAndWaitMessage("Submit:" + words.getText());
			String[] arguments = responce.split(":");
			msg.setText(arguments[0]);
			points.setText(arguments[1]);
			words.setText("");
		}
	}

}
