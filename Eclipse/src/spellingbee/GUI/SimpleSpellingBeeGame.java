package spellingbee.GUI;

import spellingbee.server.ISpellingBeeGame;

/**
 * A much much simpler version of SpellingBeeGame to initally test out
 * GameTab.java
 * @author Phan Hieu Nghia
 */


public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	
	private char l1;
	private char l2;
	private char l3;
	private char l4;
	private char l5;
	private char l6;
	private char l7;
	
	private int score;
	private String attempt;
	
	
	public SimpleSpellingBeeGame(char l1, char l2, char l3, char l4, char l5, char l6, char l7) {
		this.l1 = 'i';
		this.l2 = 'u';
		this.l3 = 'c';
		this.l4 = 'l';
		this.l5 = 's';
		this.l6 = 'b';
		this.l7 = 'm';
	}

	@Override
	public int getPointsForWord(String attempt) {
		return 1;
	}

	@Override
	public String getMessage(String attempt) {
		return "You win";
	}

	@Override
	public String getAllLetters() {
		String pangram = "iuclsbm";
		return pangram;
	}

	@Override
	public char getCenterLetter() {
		this.l4 = 'l';
		return l4;
	}

	@Override
	public int getScore() {
		return 1;
	}

	@Override
	public int[] getBrackets() {
		int[] arr = new int[5];
		arr[0] = 50;
		arr[1] = 100;
		arr[2] = 150;
		arr[3] = 200;
		arr[4] = 250;
		return arr;
	}
	
}
