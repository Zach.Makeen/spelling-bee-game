package spellingbee.GUI;

import javafx.geometry.Pos;
import spellingbee.network.Client;
import javafx.scene.paint.*; 
import javafx.scene.control.*; 
import javafx.scene.layout.*;
/**
 * @author Phan Hieu Nghia
 */
public class GameTab extends Tab {
	
	//private fields
	private Client client;
	private GameEngine action;
	private TextField field = new TextField();
	private Label chosenWord = new Label();
	private Label point = new Label();
	
	public GameTab(Client client) {

		super("Game");
		this.client = client;
		
		//Letters array
		String[] letters = client.sendAndWaitMessage("getAllLetters").split("");
		String centerL = client.sendAndWaitMessage("getCenterLetter");
		
		//Letters buttons
		Button[] Bletters = new Button[6];
		Button Bcenter = null;
		int counter = 0;
		
		//Loops to create seven letter buttons
		for (int i = 0; i < letters.length; i++) {
			if (letters[i].equals(centerL)) {
				Bcenter = new Button(letters[i]);
				action = new GameEngine(field, centerL, "setLetters");
				Bcenter.setOnAction(action);
				
			} else {
				Bletters[counter] = new Button(letters[i]);
				action = new GameEngine(field, letters[i], "setLetters");
				Bletters[counter].setOnAction(action);
				counter++;
			}
		}
		Bcenter.setStyle("-fx-text-fill: red");
		
		//Textfield where the typed/selected text will go
		field.setPrefWidth(200);
		field.setStyle("-fx-border-color: black");
		
		//Submit, clear and delete buttons
		Button submit = new Button("Submit");
		action = new GameEngine(chosenWord, point, field, client, "Submit");
		submit.setOnAction(action);
		
		Button clear = new Button("Clear");
		action = new GameEngine(field, "Clear");
		clear.setOnAction(action);
	
		Button delete = new Button("Delete");
		action = new GameEngine(field, "Delete");
		delete.setOnAction(action);
		
		//Labels styling
		chosenWord.setStyle("-fx-padding: 5 10; -fx-border-color: black");
		chosenWord.setPrefWidth(175);
		chosenWord.setTextFill(Color.web("#000000"));
		chosenWord.setWrapText(true);
		chosenWord.setPrefHeight(50);

		point.setStyle("-fx-padding: 5 10; -fx-border-color: black");
		point.setTextFill(Color.web("#000000"));
		point.setPrefWidth(175);
		point.setPrefHeight(50);
		
		//GUI skeleton
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox submission = new HBox();
		HBox text = new HBox();
		
		buttons.getChildren().addAll(Bletters[0],Bletters[1],Bletters[2],Bcenter,Bletters[3],Bletters[4],Bletters[5]);
		buttons.setAlignment(Pos.CENTER);
		submission.getChildren().addAll(submit, clear, delete);
		submission.setAlignment(Pos.CENTER);
		text.getChildren().addAll(chosenWord, point);
		text.setAlignment(Pos.CENTER);

		
		overall.getChildren().addAll(buttons, field, submission, text);
		
		this.setContent(overall);
	}
	public Label getScoreField() {
		
		return this.point;
	}
}
