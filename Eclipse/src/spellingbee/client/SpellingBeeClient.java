package spellingbee.client;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import spellingbee.GUI.GameTab;
import spellingbee.GUI.ScoreTab;
import spellingbee.network.*;
/**
 * @author Zacharie Makeen & Phan Hieu Nghia
 */
public class SpellingBeeClient extends Application {

	private Client client = new Client();
	/**
	 * The main method launches the gui.
	 */
	public static void main(String[] args) {
		
		launch(args);
	}
	/**
	 * This method constructs the stage for the gui which will for both
	 * the game tab and the score tab. This is where the user will
	 * interact with the gui to play the game.
	 * 
	 * @param stage the stage that holds each javafx object.
	 */
	@Override
	public void start(Stage stage) {

		TabPane tab = new TabPane();
		GameTab game = new GameTab(this.client);
		ScoreTab score = new ScoreTab(this.client);
		
		tab.getTabs().add(game);
		tab.getTabs().add(score);
		tab.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Group root = new Group(tab);
	
		Scene scene = new Scene(root, 400, 400); 
		scene.setFill(Color.WHITE);
		
		Label points = game.getScoreField();
		points.textProperty().addListener((ov, t, t1) -> {
			
			score.refresh();
		});
		
		stage.setTitle("Spelling Bee");
		stage.setScene(scene); 
		stage.show();
	}
}
