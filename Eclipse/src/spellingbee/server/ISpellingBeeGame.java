package spellingbee.server;
/**
 * @author Zacharie Makeen & Phan Hieu Nghia
 */
public interface ISpellingBeeGame {

	public int getPointsForWord(String attempt);
	public String getMessage(String attempt);
	public String getAllLetters();
	public char getCenterLetter();
	public int getScore();
	public int[] getBrackets();
}
