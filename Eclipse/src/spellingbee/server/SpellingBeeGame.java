package spellingbee.server;

import java.util.HashSet;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;
/**
 * @author Zacharie Makeen
 */
public class SpellingBeeGame implements ISpellingBeeGame {

	private String letters;
	private int centerLetterIndex;
	private int currentScore;
	private int maxScore;
	private HashSet<String> wordsFound = new HashSet<String>();
	public static HashSet<String> possibleWords = createWordsFromFile();
	private Random number = new Random();
	/**
	 * This constructor initializes the values of the private fields:
	 * letters, centerLetterIndex, possibleWords & maxScore.
	 */
	public SpellingBeeGame() {
		
		this.letters = getLetterCombination();
		this.centerLetterIndex = number.nextInt(this.letters.length());
		possibleWords = findPossibleWordCombinations();
		this.maxScore = calculateMaxPoints();
	}
	/**
	 * This constructor initializes the values of the private fields:
	 * letters, centerLetterIndex, possibleWords & maxScore.
	 *
	 * @param letterCombination a string representing the combination of
	 * 							letters used by the user to attempt to
	 * 							create words.
	 */
	public SpellingBeeGame(String letterCombination) {
		
		this.letters = letterCombination;
		this.centerLetterIndex = number.nextInt(this.letters.length());
		possibleWords = findPossibleWordCombinations();
		this.maxScore = calculateMaxPoints();
	}
	/**
	 * This method determines how many points a word is worth and returns
	 * that number.
	 *
	 * @param attempt a word from the possible words.
	 * @return 	      the number of points that word is worth.
	 */
	public int getPointsForWord(String attempt) {

		int attemptPoints = 0;
		if(!(wordsFound.contains(attempt))) {

			if(possibleWords.contains(attempt)) {

				if(checkPangram(attempt)) {

					return (attempt.length() + 7);
				} else if(attempt.length() == 4) {

					attemptPoints = 1;
				} else if(attempt.length() > 4) {

					attemptPoints = attempt.length();
				}
			}
		}
		this.currentScore += attemptPoints;
		this.wordsFound.add(attempt);
		return attemptPoints;
	}
	/**
	 * This method determines what message to return based on the
	 * condition of the word that the user wrote.
	 *
	 * @param attempt a string representing the word written by the user.
	 * @return 		  a message determined by the word the user wrote.
	 */
	public String getMessage(String attempt) {

		String message = "";

		if(!(wordsFound.contains(attempt))) {

			if(attempt.contains(Character.toString(this.letters.charAt(this.centerLetterIndex)))) {

				if(possibleWords.contains(attempt)) {

					message = "Nice one!";
				} else if(attempt.length() < 4) {

					message = "Too short!";
				} else {

					message = "Word does not exist.";
				}
			} else {

				message = "Word does not contain center letter!";
			}
		} else {

			message = "Already guessed this word.";
		}
		return message;
	}
	/**
	 * Returns the letters field.
	 *
	 * @return the letter set.
	 */
	public String getAllLetters() {

		return this.letters;
	}
	/**
	 * Returns the center letter.
	 *
	 * @return the center letter.
	 */
	public char getCenterLetter() {

		return this.letters.charAt(this.centerLetterIndex);
	}
	/**
	 * Returns the current user's score.
	 *
	 * @return the current score.
	 */
	public int getScore() {

		return this.currentScore;
	}
	/**
	 * This method set up each milestone by taking into consideration the
	 * maximum amount of points attainable.
 	 *
	 * @return brackets an integer array holding each milestone for each
	 * 					rank depending on max score that is attainable.
	 */
	public int[] getBrackets() {

		int[] brackets = new int[5];
		brackets[0] = (int) (maxScore * 0.25);
		brackets[1] = (int) (maxScore * 0.50);
		brackets[2] = (int) (maxScore * 0.75);
		brackets[3] = (int) (maxScore * 0.90);
		brackets[4] = (maxScore);
		return brackets;
	}
	/**
	 * This method takes each line from the english.txt file and transfers
	 * them to a HashSet<String>.
	 *
	 * @return englishWords a HashSet<String> holding each english word
	 * from the english.txt file.
	 * 
	 */
	private static HashSet<String> createWordsFromFile() {
		Path pathToEnglishFile = Paths.get("C:\\Program Files\\Java\\spelling-bee\\english.txt");
		HashSet<String> englishWords = new HashSet<String>();
		try {
			
			englishWords.addAll(Files.readAllLines(pathToEnglishFile));
		} catch(IOException e) {
			
			e.printStackTrace();
		}
		return englishWords;
	}
	/**
	 * Using the center letter from the letter set, we flush out all the
	 * the words that do not contain that center letter. We then flush
	 * again to remove the words that contain letters existing outside of
	 * the letter set.
	 * 
	 * @return the word combinations that can be made from the letter set.
	 */
	private HashSet<String> findPossibleWordCombinations() {

		HashSet<String> possibleEnglishWords = new HashSet<String>();
		for(String word : possibleWords) {

			if(word.contains(Character.toString(this.letters.charAt(this.centerLetterIndex))) && word.length() >= 4) {

				int count = 0;
				for(int i = 0; i < word.length(); i++) {

					if(this.letters.contains(Character.toString(word.charAt(i)))) {

						count++;
					}
				}
				if(count == word.length()) {

					possibleEnglishWords.add(word);
				}	
			}
		}
		return possibleEnglishWords;
	}
	/**
	 * This method take each line from the letterCombinations.txt file and
	 * transfers them to a List object. Then it randomly chooses a line
	 * out of the 501 lines and returns it.
	 *
	 * @return a random letter set from the letterCombinations.txt file.
	 */
	private String getLetterCombination() {

		Path pathToLetterCombinationsFile = Paths.get("C:\\Program Files\\Java\\spelling-bee\\letterCombinations.txt");
		List<String> letterCombinations = new ArrayList<String>();
		try {
			
			letterCombinations = Files.readAllLines(pathToLetterCombinationsFile);
		} catch(IOException e) {
			
			e.printStackTrace();
		}
		return (letterCombinations.get(number.nextInt(letterCombinations.size())));
	}
	/**
	 * This method iterates through each possible word and calls the
	 * calculateWordPoints method to calculate the worth of each word and
	 * stores it to a variable.
	 *
	 * @return the maximum amount of points attainable from the letter set.
	 */
	private int calculateMaxPoints() {

		int totalPoints = 0;
		for(String word : possibleWords) {

			totalPoints += calculateWordPoints(word);
		}
		return totalPoints;
	}
	/**
	 * This method determines how many points a word is worth and returns
	 * that number.
	 *
	 * @param word a word from the possible words.
	 * @return 	   the number of points that word is worth.
	 */
	private int calculateWordPoints(String word) {

		int wordPoints = 0;
		if(checkPangram(word)) {

			return (word.length() + 7);
		} else if(word.length() == 4) {

			wordPoints = 1;
		} else if(word.length() > 4) {

			wordPoints = word.length();
		}
		return wordPoints;
	}
	/**
	 * This method iterates through each letter from the letter set in
	 * order to check if the word is a pangram (A word that uses all the
	 * available letters). 
	 *
	 * @param word a word from the possible words.
	 * @return     a boolean representing whether or not the word is a
	 * 			   pangram.
	 */
	private boolean checkPangram(String word) {

		int pangramCount = 0;
		for(int i = 0; i < this.letters.length(); i++) {

			if(word.contains(Character.toString(this.letters.charAt(i)))) {

				pangramCount++;
			}
		}
		return (pangramCount == this.letters.length());
	}
}